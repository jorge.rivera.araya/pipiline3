
import {Suma} from '../Funciones'
import React from 'react'
import App from '../App'
import Enzyme,{mount,shallow} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
Enzyme.configure({adapter:new Adapter()})

describe('PRUEBA DE COMPONENTE',()=>{
    test('CONTENIDO',()=>{
        const wrapper = mount(<App/>)
        const h1 = wrapper.find('h1');
        expect(h1.text()).toBe('Pipeline FrontEnd')
    })
});

describe('PRUEBA DE FUNCION',()=>{
    test('SUMA', () => {
        expect(Suma(3,4)).toBe(7);
    });
});